//shared

function youSayModif(string) {
    let modifiedText;

    const regex = /> You say \"(.*(\n.*)*)*\"/i;
    const youDoubleDot = "\n> You: ";

    var youSay = regex.exec(string);
    if (string.match(regex)) {
        modifiedText = youDoubleDot + youSay[1];
        //test debug > You say "Hello"
    }
    return {
        text: modifiedText
    }
}

//input

const modifier = (text) => {
    let modifiedText = text;

    if (text.match("You say")) {
        modifiedText = youSayModif(text).text
    } else {
        modifiedText = text;
    }
    return {
        text: modifiedText
    }
}

// Don't modify this part
modifier(text)
