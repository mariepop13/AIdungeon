//shared

function perspective(string) {
    let modifiedText = text
        const lowered = text.toLowerCase()

        var patt = /(\> You( say)*)* *\"* *(-.*-) *\"*.*/m
        var matchBool = patt.test(text);
    var matchStr = patt.exec(text);

    if (matchBool) {
        modifiedText = text.replace(matchStr[0],
                `\n--
		\n${matchStr[3]}
		\n--
		\n`)
    }
    return {
        text: modifiedText
    }
}

//input

const modifier = (text) => {
    let modifiedText = text;

    if (text.match(/-.*-/)) {
        modifiedText = perspective(text).text
    } else {
        modifiedText = text;
    }
    return {
        text: modifiedText
    }
}

// Don't modify this part
modifier(text)

//Open in the AI Dungeon app:
https: //aidungeon.page.link/?link=https://scenarioView?publicId=2217a100-01be-11eb-8ffa-8362806555cd&ofl=https%3A%2F%2Fplay.aidungeon.io%2Fmain%2FscenarioView%3FpublicId%3D2217a100-01be-11eb-8ffa-8362806555cd&apn=com.aidungeon&ibi=com.aidungeon.app&isi=1491268416

//Open at the AI Dungeon website:
https: //play.aidungeon.io/main/scenarioView?publicId=2217a100-01be-11eb-8ffa-8362806555cd
