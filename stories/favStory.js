//shared

function youSayModif(string) {
    let modifiedText;

    const regex = /> You say \"(.*(\n.*)*)*\"/i;
    const youDoubleDot = "\n> You: ";

    var youSay = regex.exec(string);
    if (string.match(regex)) {
        modifiedText = youDoubleDot + youSay[1];
        //test debug > You say "Hello"
    }
    return {
        text: modifiedText
    }
}

function perspective(string) {
    let modifiedText = text
        const lowered = text.toLowerCase()

        var patt = /(\> You( say)*)* *\"* *(-.*-) *\"*.*/m
        var matchBool = patt.test(text);
    var matchStr = patt.exec(text);

    if (matchBool) {
        modifiedText = text.replace(matchStr[0],
                `\n--
		\n${matchStr[3]}
		\n--
		\n`)
    }
    return {
        text: modifiedText
    }
}

//input

const modifier = (text) => {
    let modifiedText = text;

    if (text.match(/-.*-/)) {
        modifiedText = perspective(text).text
    } else if (text.match(/> You say/)) {
        modifiedText = youSayModif(text).text
    } else {
        modifiedText = text;
    }
    return {
        text: modifiedText
    }
}

// Don't modify this part
modifier(text)
