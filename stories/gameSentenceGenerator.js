//shared

function youSayModif(string) {
    let modifiedText;

    const regex = /> You say \"(.*(\n.*)*)*\"/i;
    const youDoubleDot = "\n> You: ";

    var youSay = regex.exec(string);
    if (string.match(regex)) {
        modifiedText = youDoubleDot + youSay[1] + "\n";
        //test debug > You say "Hello"
    }
    return {
        text: modifiedText
    }
}

//input

const modifier = (text) => {
    let modifiedText = text;

    if (text.match("You say")) {
        modifiedText = youSayModif(text).text
    } else {
        modifiedText = text;
    }
    return {
        text: modifiedText
    }
}

// Don't modify this part
modifier(text)

//output

const modifier = (text) => {
    let modifiedText = text
        const lowered = text.toLowerCase()

        var patt = /((2\. *)*(You answer)) *((\n.*)*)/
        var matchBool = patt.test(text);
    var matchStr = patt.exec(text);

    if (matchBool) {
        modifiedText = text.replace(matchStr[4], " ");
    } else {
        modifiedText = text;
    }

    console.log(modifiedText)
    // You must return an object with the text property defined.
    return {
        text: modifiedText
    }
}

// Don't modify this part
modifier(text)
