//shared


//input

const modifier = (text) => {
    let modifiedText = text
        const lowered = text.toLowerCase()

        // You must return an object with the text property defined.
        return {
        text: modifiedText
    }
}

// Don't modify this part
modifier(text)

//output

const modifier = (text) => {
    let modifiedText = text
        const lowered = text.toLowerCase()

        // You must return an object with the text property defined.
        return {
        text: modifiedText
    }
}

// Don't modify this part
modifier(text)

//context

// info.memoryLength is the length of the memory section of text.
// info.maxChars is the maximum length that text can be. The server will truncate the text you return to this length.

// This modifier re-implements Author's Note as an example.
const modifier = (text) => {

    return {
        text: finalText
    }
}

// Don't modify this part
modifier(text)

//in progress


